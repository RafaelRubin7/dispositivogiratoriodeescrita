int redpin0=52;      //Pin 52
int greenpin0=50;    //Pin 50
int bluepin0=48;      //Pin 48

int redpin1=46;      //Pin 46
int greenpin1=44;    //Pin 44
int bluepin1=42;      //Pin 42

int redpin2=40;      //Pin 40
int greenpin2=38;    //Pin 38
int bluepin2=36;      //Pin 36

int redpin3=34;      //Pin 34
int greenpin3=32;    //Pin 32
int bluepin3=30;      //Pin 30

int redpin4=28;      //Pin 28
int greenpin4=26;    //Pin 26
int bluepin4=24;      //Pin 24

int redpin5=22;      //Pin 22
int greenpin5=13;    //Pin 13
int bluepin5=12;      //Pin 12

int redpin6=11;      //Pin 11
int greenpin6=10;    //Pin 10
int bluepin6=9;      //Pin 9

int redpin7=8;      //Pin 8
int greenpin7=7;    //Pin 7
int bluepin7=6;      //Pin 6

int redpin8=5;      //Pin 5
int greenpin8=4;    //Pin 4
int bluepin8=3;      //Pin 3

int redpin9= A14;      //Pin A14
int greenpin9=23;    //Pin 23
int bluepin9=25;      //Pin 25

int redpin10=27;      //Pin 27
int greenpin10=29;    //Pin 29
int bluepin10=31;      //Pin 31

int redpin11=33;      //Pin 33
int greenpin11=35;    //Pin 35
int bluepin11=37;      //Pin 37

int redpin12=39;      //Pin 39
int greenpin12=41;    //Pin 41
int bluepin12=43;      //Pin 43

int redpin13=45;      //Pin 45
int greenpin13=47;    //Pin 47
int bluepin13=49;      //Pin 49

int redpin14=51;      //Pin 51
int greenpin14=53;    //Pin 53
int bluepin14=A15;      //Pin A15

int vet1[25], vet2[25];
int letras1[25], letras2[25];
int tam1 = 0, tam2 = 0, aux1 = 0, aux2 = 0, var = 0, var1 = 0;
int ini1, ini2, b, n, m, porta, a, i, j, cont;
char valor;
unsigned long dif, tempo = 0, now, dif_a, grau = 0, tempo_1grau;
float k1 = 0.3, k2 = 0.7;
int cor = 3;


const byte alfabeto[37][7][15] PROGMEM = {
  { //espaco
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra A
    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
  },
  { //Letra B
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0}
  },
  { //Letra C
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
  },
  { //Letra D
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0}
  },
  { //Letra E
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
  },
  { //Letra F
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra G
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
  },
  { //Letra H
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
  },
  { //Letra I
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra J
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra K
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}
  },
  { //Letra L
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
  },
  { //Letra M
    {1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
  },
  { //Letra N
    {0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0}
  },
  { //Letra O
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0}
  },
  { //Letra P
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra Q
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}
  },
  { //Letra R
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
  },
  { //Letra S
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0}
  },
  { //Letra T
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra U
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
  },
  { //Letra V
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Letra W
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0}
  },
  { //Letra X
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
  },
  { //Letra Y
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0}
  },
  { //Letra Z
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0}
  },
  { //Numero 0
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0}
  },
  { //Numero 1
    {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}
  },
  { //Numero 2
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0}
  },
  { //Numero 3
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0}
  },
  { //Numero 4
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}
  },
  { //Numero 5
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0}
  },
  { //Numero 6
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0}
  },
  { //Numero 7
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}
  },
  { //Numero 8
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0}
  },
  { //Numero 9
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0}
  }
};

//Esta função é responsável por ler o valor da matriz alfabeto[][][] e escrever o valor na porta de saída do arduino
void escreve (int porta, int nl, int l, int c)
{
  memcpy_P(&valor,  &alfabeto[nl][l][c], sizeof valor);
  if (valor == 1)  digitalWrite(porta, HIGH);
  else digitalWrite(porta, LOW);
}

//Esta função serve para tratar a interrupção causada pelo sensor infra-vermelho
void interrupcao() {
  aux1 = 0;
  aux2 = 0;
  j = 0;
  m = 0;
  grau = 0;
  now = micros();
  dif_a = dif;
  dif = (now - tempo) * k1 + dif * k2 ; // Calculo para evitar oscilações causadas pela mudança de velocidade
  if ( (tempo_1grau >= 2) && (tempo_1grau < 510) )
  {
    cont = int( 255 - tempo_1grau * (16.0 / 32));
    TCNT2 = cont;
  }
  else
  {
    cont = 0;
    TCNT2 = 0;
  }
  tempo = now;
  tempo_1grau = dif / 360 ;
}


ISR(TIMER2_OVF_vect)  //Essa função é chamada quando ocorre a interrupção por tempo
{
  TCNT2 = cont;            // preload timer
  //Escreve 0 em todas as portas do Arduino
  PORTA = B00000000;
  PORTB = B00000000;
  PORTC = B00000000;
  PORTD = B00000000;
  PORTE = B00000000;
  PORTF = B00000000;
  PORTG = B00000000;
  PORTH = B00000000;
  PORTJ = B00000000;
  PORTK = B00000000;
  PORTL = B00000000;

switch (cor){
case 0: //red
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(redpin0, a, i, j);
          break;

        case 1:
          escreve(redpin1, a, i, j);
          break;

        case 2:
          escreve(redpin2, a, i, j);
          break;

        case 3:
          escreve(redpin3, a, i, j);
          break;

        case 4:
          escreve(redpin4, a, i, j);
          break;

        case 5:
          escreve(redpin5, a, i, j);
          break;

        case 6:
          escreve(redpin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(redpin8, b, n, m);
          break;

        case 1:
          escreve(redpin9, b, n, m);
          break;

        case 2:
          escreve(redpin10, b, n, m);
          break;

        case 3:
          escreve(redpin11, b, n, m);
          break;

        case 4:
          escreve(redpin12, b, n, m);
          break;

        case 5:
          escreve(redpin13, b, n, m);
          break;

        case 6:
          escreve(redpin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;


case 1: //green
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(greenpin0, a, i, j);
          break;

        case 1:
          escreve(greenpin1, a, i, j);
          break;

        case 2:
          escreve(greenpin2, a, i, j);
          break;

        case 3:
          escreve(greenpin3, a, i, j);
          break;

        case 4:
          escreve(greenpin4, a, i, j);
          break;

        case 5:
          escreve(greenpin5, a, i, j);
          break;

        case 6:
          escreve(greenpin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(greenpin8, b, n, m);
          break;

        case 1:
          escreve(greenpin9, b, n, m);
          break;

        case 2:
          escreve(greenpin10, b, n, m);
          break;

        case 3:
          escreve(greenpin11, b, n, m);
          break;

        case 4:
          escreve(greenpin12, b, n, m);
          break;

        case 5:
          escreve(greenpin13, b, n, m);
          break;

        case 6:
          escreve(greenpin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;

  case 2: //blue
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(bluepin0, a, i, j);
          break;

        case 1:
          escreve(bluepin1, a, i, j);
          break;

        case 2:
          escreve(bluepin2, a, i, j);
          break;

        case 3:
          escreve(bluepin3, a, i, j);
          break;

        case 4:
          escreve(bluepin4, a, i, j);
          break;

        case 5:
          escreve(bluepin5, a, i, j);
          break;

        case 6:
          escreve(bluepin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(bluepin8, b, n, m);
          break;

        case 1:
          escreve(bluepin9, b, n, m);
          break;

        case 2:
          escreve(bluepin10, b, n, m);
          break;

        case 3:
          escreve(bluepin11, b, n, m);
          break;

        case 4:
          escreve(bluepin12, b, n, m);
          break;

        case 5:
          escreve(bluepin13, b, n, m);
          break;

        case 6:
          escreve(bluepin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;

  case 3: //azul claro
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(bluepin0, a, i, j);
          escreve(greenpin0, a, i, j);
          break;

        case 1:
          escreve(bluepin1, a, i, j);
          escreve(greenpin1, a, i, j);
          break;

        case 2:
          escreve(bluepin2, a, i, j);
          escreve(greenpin2, a, i, j);
          break;

        case 3:
          escreve(bluepin3, a, i, j);
          escreve(greenpin3, a, i, j);
          break;

        case 4:
          escreve(bluepin4, a, i, j);
          escreve(greenpin4, a, i, j);
          break;

        case 5:
          escreve(bluepin5, a, i, j);
          escreve(greenpin5, a, i, j);
          break;

        case 6:
          escreve(bluepin6, a, i, j);
          escreve(greenpin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(bluepin8, b, n, m);
          escreve(greenpin8, b, n, m);
          break;

        case 1:
          escreve(bluepin9, b, n, m);
          escreve(greenpin9, b, n, m);
          break;

        case 2:
          escreve(bluepin10, b, n, m);
          escreve(greenpin10, b, n, m);
          break;

        case 3:
          escreve(bluepin11, b, n, m);
          escreve(greenpin11, b, n, m);
          break;

        case 4:
          escreve(bluepin12, b, n, m);
          escreve(greenpin12, b, n, m);
          break;

        case 5:
          escreve(bluepin13, b, n, m);
          escreve(greenpin13, b, n, m);
          break;

        case 6:
          escreve(bluepin14, b, n, m);
          escreve(greenpin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;

  case 4: //lilás
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(bluepin0, a, i, j);
          escreve(redpin0, a, i, j);
          break;

        case 1:
          escreve(bluepin1, a, i, j);
          escreve(redpin1, a, i, j);
          break;

        case 2:
          escreve(bluepin2, a, i, j);
          escreve(redpin2, a, i, j);
          break;

        case 3:
          escreve(bluepin3, a, i, j);
          escreve(redpin3, a, i, j);
          break;

        case 4:
          escreve(bluepin4, a, i, j);
          escreve(redpin4, a, i, j);
          break;

        case 5:
          escreve(bluepin5, a, i, j);
          escreve(redpin5, a, i, j);
          break;

        case 6:
          escreve(bluepin6, a, i, j);
          escreve(redpin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(bluepin8, b, n, m);
          escreve(redpin8, b, n, m);
          break;

        case 1:
          escreve(bluepin9, b, n, m);
          escreve(redpin9, b, n, m);
          break;

        case 2:
          escreve(bluepin10, b, n, m);
          escreve(redpin10, b, n, m);
          break;

        case 3:
          escreve(bluepin11, b, n, m);
          escreve(redpin11, b, n, m);
          break;

        case 4:
          escreve(bluepin12, b, n, m);
          escreve(redpin12, b, n, m);
          break;

        case 5:
          escreve(bluepin13, b, n, m);
          escreve(redpin13, b, n, m);
          break;

        case 6:
          escreve(bluepin14, b, n, m);
          escreve(redpin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;

  case 5: //amarelo
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(redpin0, a, i, j);
          escreve(greenpin0, a, i, j);
          break;

        case 1:
          escreve(redpin1, a, i, j);
          escreve(greenpin1, a, i, j);
          break;

        case 2:
          escreve(redpin2, a, i, j);
          escreve(greenpin2, a, i, j);
          break;

        case 3:
          escreve(redpin3, a, i, j);
          escreve(greenpin3, a, i, j);
          break;

        case 4:
          escreve(redpin4, a, i, j);
          escreve(greenpin4, a, i, j);
          break;

        case 5:
          escreve(redpin5, a, i, j);
          escreve(greenpin5, a, i, j);
          break;

        case 6:
          escreve(redpin6, a, i, j);
          escreve(greenpin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(redpin8, b, n, m);
          escreve(greenpin8, b, n, m);
          break;

        case 1:
          escreve(redpin9, b, n, m);
          escreve(greenpin9, b, n, m);
          break;

        case 2:
          escreve(redpin10, b, n, m);
          escreve(greenpin10, b, n, m);
          break;

        case 3:
          escreve(redpin11, b, n, m);
          escreve(greenpin11, b, n, m);
          break;

        case 4:
          escreve(redpin12, b, n, m);
          escreve(greenpin12, b, n, m);
          break;

        case 5:
          escreve(redpin13, b, n, m);
          escreve(greenpin13, b, n, m);
          break;

        case 6:
          escreve(redpin14, b, n, m);
          escreve(greenpin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;

  case 6: //branca
  if (grau >= vet1[aux1] && grau <= (vet1[aux1] + 14))
  {

    a = letras1[aux1];

    for (i = 0; i < 7; i++)
    {
      switch (i)
      {
        case 0:
          escreve(redpin0, a, i, j);
          escreve(greenpin0, a, i, j);
          escreve(bluepin0, a, i, j);
          break;

        case 1:
          escreve(redpin1, a, i, j);
          escreve(greenpin1, a, i, j);
          escreve(bluepin1, a, i, j);
          break;

        case 2:
          escreve(redpin2, a, i, j);
          escreve(greenpin2, a, i, j);
          escreve(bluepin2, a, i, j);
          break;

        case 3:
          escreve(redpin3, a, i, j);
          escreve(greenpin3, a, i, j);
          escreve(bluepin3, a, i, j);
          break;

        case 4:
          escreve(redpin4, a, i, j);
          escreve(greenpin4, a, i, j);
          escreve(bluepin4, a, i, j);
          break;

        case 5:
          escreve(redpin5, a, i, j);
          escreve(greenpin5, a, i, j);
          escreve(bluepin5, a, i, j);
          break;

        case 6:
          escreve(redpin6, a, i, j);
          escreve(greenpin6, a, i, j);
          escreve(bluepin6, a, i, j);
          break;
      }
    }
    j++;

    if (grau == (vet1[aux1] + 14) )
    {
      if (aux1 < (tam1 - 1))
      {
        aux1++;
      }

      j = 0;
    }

  }

  if (grau >= vet2[aux2] && grau <= (vet2[aux2] + 14))
  {
    b = letras2[aux2];

    for (n = 0; n < 7; n++)
    {
      switch (n)
      {
        case 0:
          escreve(redpin8, b, n, m);
          escreve(greenpin8, b, n, m);
          escreve(bluepin8, b, n, m);
          break;

        case 1:
          escreve(redpin9, b, n, m);
          escreve(greenpin9, b, n, m);
          escreve(bluepin9, b, n, m);
          break;

        case 2:
          escreve(redpin10, b, n, m);
          escreve(greenpin10, b, n, m);
          escreve(bluepin10, b, n, m);
          break;

        case 3:
          escreve(redpin11, b, n, m);
          escreve(greenpin11, b, n, m);
          escreve(bluepin11, b, n, m);
          break;

        case 4:
          escreve(redpin12, b, n, m);
          escreve(greenpin12, b, n, m);
          escreve(bluepin12, b, n, m);
          break;

        case 5:
          escreve(redpin13, b, n, m);
          escreve(greenpin13, b, n, m);
          escreve(bluepin13, b, n, m);
          break;

        case 6:
          escreve(redpin14, b, n, m);
          escreve(greenpin14, b, n, m);
          escreve(bluepin14, b, n, m);
          break;
      }
    }
    m++;

    if (grau == (vet2[aux2] + 14) )
    {
      if (aux2 < (tam2 - 1))
      {
        aux2++;
      }
      m = 0;

    }
  }
  grau++;
  break;  
}
}


void setup() {
 
  noInterrupts();           // Desativa as interrupcoes
  TCCR2A = 0;               // Configurações do Timer2
  TCCR2B = 0;               // Configurações do Timer2
  TCNT2 = 0;                // Define o contador em 0 de 255
  TCCR2B = 3;               // Prescaler de 32
  TIMSK2 |= (1 << TOIE1);   // Ativa timer overflow interrupt
  interrupts();             // Ativa as interrupcoes


  attachInterrupt(0, interrupcao, FALLING ); //Configurando a interrupção
  pinMode(redpin0, OUTPUT);
  pinMode(redpin1, OUTPUT);
  pinMode(redpin2, OUTPUT);
  pinMode(redpin3, OUTPUT);
  pinMode(redpin4, OUTPUT);
  pinMode(redpin5, OUTPUT);
  pinMode(redpin6, OUTPUT);
  pinMode(redpin7, OUTPUT);
  pinMode(redpin8, OUTPUT);
  pinMode(redpin9, OUTPUT);
  pinMode(redpin10, OUTPUT);
  pinMode(redpin11, OUTPUT);
  pinMode(redpin12, OUTPUT);
  pinMode(redpin13, OUTPUT);
  pinMode(redpin14, OUTPUT);
  pinMode(greenpin0, OUTPUT);
  pinMode(greenpin1, OUTPUT);
  pinMode(greenpin2, OUTPUT);
  pinMode(greenpin3, OUTPUT);
  pinMode(greenpin4, OUTPUT);
  pinMode(greenpin5, OUTPUT);
  pinMode(greenpin6, OUTPUT);
  pinMode(greenpin7, OUTPUT);
  pinMode(greenpin8, OUTPUT);
  pinMode(greenpin9, OUTPUT);
  pinMode(greenpin10, OUTPUT);
  pinMode(greenpin11, OUTPUT);
  pinMode(greenpin12, OUTPUT);
  pinMode(greenpin13, OUTPUT);
  pinMode(greenpin14, OUTPUT);
  pinMode(bluepin0, OUTPUT);
  pinMode(bluepin1, OUTPUT);
  pinMode(bluepin2, OUTPUT);
  pinMode(bluepin3, OUTPUT);
  pinMode(bluepin4, OUTPUT);
  pinMode(bluepin5, OUTPUT);
  pinMode(bluepin6, OUTPUT);
  pinMode(bluepin7, OUTPUT);
  pinMode(bluepin8, OUTPUT);
  pinMode(bluepin9, OUTPUT);
  pinMode(bluepin10, OUTPUT);
  pinMode(bluepin11, OUTPUT);
  pinMode(bluepin12, OUTPUT);
  pinMode(bluepin13, OUTPUT);
  pinMode(bluepin14, OUTPUT);


  //Escreve a palavra ENGENHARIA 2019
  letras1[0] = 5;
  letras1[1] = 14;
  letras1[2] = 7;
  letras1[3] = 5;
  letras1[4] = 14;
  letras1[5] = 8;
  letras1[6] = 1;
  letras1[7] = 18;
  letras1[8] = 9;
  letras1[9] = 1;

  tam1 = 10;

  vet1[0] = 90;
  vet1[1] = 110;
  vet1[2] = 130;
  vet1[3] = 150;
  vet1[4] = 170;
  vet1[5] = 190;
  vet1[6] = 210;
  vet1[7] = 230;
  vet1[8] = 250;
  vet1[9] = 270;


  letras2[0] = 29;
  letras2[1] = 27;
  letras2[2] = 28;
  letras2[3] = 36;


  tam2 = 4;

  vet2[0] = 120;
  vet2[1] = 140;
  vet2[2] = 160;
  vet2[3] = 180;


}
void loop() {
for(cor = 0 ; cor < 7 ; cor ++){ //para ficar alternando as cores
  delay(8000); 
}
}

